/* This is a basic hello world C program */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include "MemoryBuffer.h"

#define BUFSIZE (64*1024)

#define SIZE_STRING		1024

#define SIGNATUREONE	47577
#define SIGNATURETWO	77777

#define TEST_MESSAGE	10001
#define TEST_REPLY		10002
#define PREPARE_FILETRANSFER	10003
#define BEGINFILESEND	10004


int StartServer ();
void FileCopy ();

main()
{
	StartServer ();
	//FileCopy ();
	/*
	printf("This is a socket server program created by Danny Draper!\n");
	
	const int listenPortNumber = 7009;

	const int listenSocketFile = socket(AF_INET, SOCK_STREAM, 0);
	
	if (listenSocketFile < 0) 
		error("ERROR opening server socket");

	struct sockaddr_in listenAddress;
	bzero((char *) &listenAddress, sizeof(listenAddress));

	listenAddress.sin_family = AF_INET;
	listenAddress.sin_addr.s_addr = INADDR_ANY;
	listenAddress.sin_port = htons(listenPortNumber);

	const int bindResult = bind(listenSocketFile, (struct sockaddr *) &listenAddress, sizeof(listenAddress));

	if (bindResult<0) 
		error("ERROR on binding");

	listen(listenSocketFile,5);

	struct sockaddr_in transferAddress;
	socklen_t sizeofTransferAddress = sizeof(transferAddress);

	const int transferSocketFile = accept(listenSocketFile, (struct sockaddr *) &transferAddress, &sizeofTransferAddress);

	if (transferSocketFile<0) 
		error("ERROR on accept");

	const int bufferLength = 256;
	char buffer[bufferLength];

	const int bytesRead = read(transferSocketFile,buffer,(bufferLength-1));
	
	if (bytesRead<0) 
		error("ERROR reading from socket");

	// Pete- On some systems the buffer past the read bytes may be altered, so make sure the string is zero terminated
	buffer[bytesRead] = '\0';

	printf("Here is the message: %s\n",buffer);

	const char* outputMessage = "I got your message";
	const int outputMessageLength = strlen(outputMessage);
	const int bytesWritten = write(transferSocketFile, outputMessage, outputMessageLength);

	if (bytesWritten<0) 
		error("ERROR writing to socket");
		
	close(transferSocketFile);
	
	close(listenSocketFile);

	return 0;
	*/
}

void *receive_cmds_ex (void* arg)
{
	printf("Client ex thread created.\n");
	
	bool bbulkmode = false;
	int current_client = *(int *)arg;
	
	int res;
	
	//				SIG1		   SIG2			SIZE
	int initbufsize = sizeof (int) + sizeof (int) + sizeof (unsigned long);
	
	MemoryBuffer initialBuffer;
	initialBuffer.SetSize (initbufsize);
	
	MemoryBuffer bulkBuffer;
	unsigned long bulkpointer = 0;
	unsigned long bulksize = 0;
	MemoryBuffer bulkStorage;
	
	int hWriteFile;
	mode_t filePerms;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	unsigned long long receivefilepointer;
	unsigned long long receivefilesize;
	MemoryBuffer replyBuffer;
	bool bfiletransfermode = false;
	MemoryBuffer fileTransferBuffer;
	unsigned long ltempsize = 1000000;
	
	while (1)
	{
		if (bfiletransfermode == true) {
			
			res = read(current_client, fileTransferBuffer.GetBuffer(), fileTransferBuffer.GetSize());
			
			if (res == -1) {
				printf("There was a socket error with id: %s\n", strerror (errno));
			}
			
			if (res == 0) {
				printf("There was an error receiving from the client. Zero bytes received.\n");
			}
			
			bool bTestwrite;
			write (hWriteFile, fileTransferBuffer.GetBuffer(), res);
			
			receivefilepointer+=res;
			
			if (receivefilepointer == receivefilesize) {
				printf("Complete file received.\n");
				close (hWriteFile);
				bfiletransfermode = false;
			}
			
			if (receivefilepointer > receivefilesize) {
				printf("File received, but file pointer is greater than file size.\n");
				close (hWriteFile);
			}
			
			
		} else { // bfiletransfermode
			
			if (bbulkmode == false) {
			
				// Non bulk mode receive - we're only expecting a signature and bulk buffer size at this stage
				res = read (current_client, initialBuffer.GetBuffer(), initialBuffer.GetSize());
				
				if (res == -1) {
					printf("There was a socket error with id: %s\n", strerror (errno));
				}
				
				if (res == 0) {
					printf("There was an error receiving from the client. Zero bytes received.\n");
				}				
				
				int sig1 = 0;
				int sig2 = 0;
				unsigned long finalbufsize = 0;
				
				unsigned long ipointer  = 0;
				memcpy (&sig1, (char *) initialBuffer.GetBuffer() + ipointer, sizeof (int));
				ipointer +=sizeof (int);
				
				memcpy (&sig2, (char *) initialBuffer.GetBuffer() + ipointer, sizeof (int));
				ipointer +=sizeof (int);
				
				memcpy (&finalbufsize, (char *) initialBuffer.GetBuffer() + ipointer, sizeof (unsigned long));
				ipointer +=sizeof (unsigned long);
				
				printf ("Res is: %i\n", res);
				printf ("Signature 1: %i\n", sig1);
				printf ("Signature 2: %i\n", sig2);
				
				if (sig1 == SIGNATUREONE && sig2 == SIGNATURETWO) {
					printf("Signature is valid.\n");
					printf("Bulk Buffer Size: %i\n", (int)finalbufsize);
					
					bulkpointer = 0;
					bulksize = finalbufsize;
					bulkBuffer.SetSize (finalbufsize);
					bulkStorage.SetSize (finalbufsize);
					bbulkmode = true;
				}
				
			} else {
			
				// We're in bulk mode - receive everything to the already set up buffer which should have been done in non-bulk mode
				res = read (current_client, bulkBuffer.GetBuffer(), bulkBuffer.GetSize ()); // Receive the data
				
				bulkStorage.Append (bulkBuffer.GetBuffer(), res);
				bulkpointer += res;
				
				printf ("Received bulk chunk. %i bytes. Pointer is %i of %i\n", res, (int) bulkpointer, (int) bulksize);
				
				if (bulkpointer == bulksize) {
					// We've received all the data
					
					printf("Entire bulk buffer received. Back to non-bulk mode.\n");
					
					int msg;
					int ipointer = 0;
					
					memcpy (&msg, (char *) bulkStorage.GetBuffer() + ipointer, sizeof (int));
					ipointer += sizeof (int);
					
					if (msg == PREPARE_FILETRANSFER) {
						
						printf ("Prepare file transfer message received.\n");
						
						unsigned long long filesize;
						
						memcpy (&filesize, (char *) bulkStorage.GetBuffer() + ipointer, sizeof (unsigned long long));
						ipointer += sizeof (unsigned long long);
						
						int fnamelen = 0;
						memcpy (&fnamelen, (char *) bulkStorage.GetBuffer() + ipointer, sizeof (int));
						ipointer += sizeof (int);
						
						char szFilename[SIZE_STRING];
						bzero (szFilename, SIZE_STRING);
						
						strncpy (szFilename, (char *) bulkStorage.GetBuffer() + ipointer, fnamelen);
						ipointer += fnamelen;
						
						printf ("Incoming file size is: %i64\n", filesize);
						printf ("Incoming filename: %s\n", szFilename);
						
						hWriteFile = open ("/tmp/receivefile.avi", O_WRONLY | O_CREAT | O_TRUNC, filePerms);
						
						receivefilepointer = 0;
						receivefilesize = filesize;
						
						fileTransferBuffer.Clear ();
						fileTransferBuffer.SetSize (ltempsize);
						
						bfiletransfermode = true;
						
						if (hWriteFile == -1) {
							printf ("There was a problem opening the new file for writing. %s\n", strerror (errno));
						}
						
						// Send the client a reply so that it can trigger the file sending
						int sig1 = SIGNATUREONE;
						int sig2 = SIGNATURETWO;
						int msgident = BEGINFILESEND;
						
						replyBuffer.Clear ();
						replyBuffer.SetSize (1000);
						replyBuffer.Append (&sig1, sizeof (int));
						replyBuffer.Append (&sig2, sizeof (int));
						replyBuffer.Append (&msgident, sizeof (int));
						
						sleep (10);
						
						write(current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize());
						
						printf("Client Reply sent and File Transfer mode has been set - awaiting data...\n");
						
					}
					
					bulkBuffer.Clear ();
					bulkStorage.Clear ();
					bulkpointer = 0;
					
					bbulkmode = false;
					
				}
			}
			
		}
	}
}

int StartServer ()
{
	printf("Starting up TCP Server....\n");
	int sock;
	pthread_t clientthread;
	
	MemoryBuffer buffer;
	
	buffer.SetSize (sizeof (int));
	
	int test = 123;
	buffer.Append (&test, sizeof (int));
	
	int testread = 0;
	
	memcpy (&testread, buffer.GetBuffer(), sizeof (int));
	printf ("Test read value is: %i\n", testread);
	
	struct sockaddr_in server;
	bzero ((char *) &server, sizeof (server));
	
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port=htons(7009);
	
	sock = socket (AF_INET, SOCK_STREAM, 0);
	
	if (sock == -1) {
		printf("There was an error creating the socket. Errno was %i\n", errno);
		return -1;
	}
	
	if (bind (sock, (struct sockaddr *) &server, sizeof (server)) != 0) {
		printf("There was an error binding the socket. Errno was %s\n", strerror (errno));
		return -1;
	}
	
	if (listen (sock, 5) != 0) {
		printf("There was an error listening. Errno was %i\n", errno);
		return -1;
	}
	
	int client;
	struct sockaddr_in from;
	socklen_t fromlen = sizeof (from);
	
	while (1)
	{
		client = accept (sock, (struct sockaddr *) &from, &fromlen);
		printf ("Client connected.\n");		
		if (pthread_create (&clientthread, NULL, receive_cmds_ex, &client) != 0) {
			printf("Error creating client thread.\n");
		} else {
			pthread_detach (clientthread);
		}
	}
	
	close (sock);
	return 0;
}

void FileCopy ()
{
	char buffer[BUFSIZE];
	int in = open ("/tmp/testfile.avi", O_RDONLY);
	mode_t filePerms;
	
	if (in == -1) {
		printf("There was a problem opening the source file for reading. %s\n", strerror (errno));
		return;
	}
	
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	
	int out = open ("/tmp/destfile.avi", O_WRONLY | O_CREAT | O_TRUNC, filePerms);
	
	if (out == -1) {
		printf("There was a problem opening the destination file for writing. %s\n", strerror (errno));
		return;
	}
	
	// Get the input file size
	struct stat st;
	fstat (in, &st);
	
	
	printf("Copying file....\n");
	ssize_t bytes_read;
	while (1)
	{
		bytes_read = read (in, buffer, BUFSIZE);
		
		if (bytes_read > 0) {
			write (out, buffer, bytes_read);
		} else {
			break;
		}
	}
	printf("Completed.\n");
	
	close (in);
	close (out);
}
