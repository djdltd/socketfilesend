// SocketFileSend.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <WinSock.h>

//Client includes
#include <iostream>
#include <conio.h>
#include <signal.h>

//Memory buffer for safe and reliable memory storage and manipulation
#include "MemoryBuffer.h"

//Socket Message Identifiers
#include "MessageIdentifiers.h"

#define BLOCKSIZE	96000


// Function headers
int StartServer ();
int StartClient ();
int StartClientEx ();

int _tmain(int argc, _TCHAR* argv[])
{
	printf ("Argument count: %i\n", argc);
	printf ("Argument contents of 0: %s\n", argv[1]);

	if (argc == 2) {
		if (strcmp (argv[1], "server") == 0) {
			printf("Server mode requested\n");
			StartServer ();
		}

		if (strcmp (argv[1], "client") == 0) {
			printf("Client mode requested\n");
			StartClient ();
		}
	}

	return 0;
	HANDLE hFile;
	HANDLE hWriteFile;

	DWORD dwNumRead;
	DWORD dwBytesWritten;
	BOOL bTest;
	BOOL bTestwrite;	
	BYTE bBuffer[BLOCKSIZE];
	LARGE_INTEGER lFileSize;
	long long lsize = 0;
	long long lpointer = 0;

	hFile = CreateFile ("C:\\Temp\\TestFile.avi", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	hWriteFile = CreateFile ("C:\\Temp\\WriteFile.avi", GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	GetFileSizeEx (hFile, &lFileSize);
	lsize = lFileSize.QuadPart;

	if (hWriteFile == INVALID_HANDLE_VALUE) {
		printf("There was a problem opening the new file for writing.\n");
	}

	if (hFile == INVALID_HANDLE_VALUE) {
		printf ("There was a problem reading the file.\n");
	} else {
		printf ("File opened ok.\n");
		printf ("File size: %i64\n", lsize);

		while (lpointer <= lsize) {
			ZeroMemory (bBuffer, BLOCKSIZE);
			
			bTest = ReadFile (hFile, bBuffer, BLOCKSIZE, &dwNumRead, NULL);			
			bTestwrite = WriteFile (hWriteFile, bBuffer, dwNumRead, &dwBytesWritten, NULL); 

			lpointer+=BLOCKSIZE;

			if (bTest == false) {
				printf ("There was a problem reading the file.\n");
			} else {
				printf ("Number of bytes read: %i\n", dwNumRead);
			}

			if (bTestwrite == false) {
				printf ("There was a problem write the new file.\n");
			} else {
				printf ("Number of bytes written: %i64\n", lpointer);
			}
		}
		
		CloseHandle (hFile);
		CloseHandle (hWriteFile);
	}

	return 0;
}

DWORD WINAPI receive_cmds_ex (LPVOID lpParam)
{
	printf("Client ex thread created.\n");

	bool bbulkmode = false;
	SOCKET current_client = (SOCKET) lpParam;

	int res;
	int lasterror = 0;
	//					SIG1		  SIG2			  SIZE
	int initbufsize = sizeof (int) + sizeof (int) + sizeof (unsigned long);

	MemoryBuffer initialBuffer;
	initialBuffer.SetSize (initbufsize);

	MemoryBuffer bulkBuffer;
	unsigned long bulkpointer = 0;
	unsigned long bulksize = 0;
	MemoryBuffer bulkStorage;


	HANDLE hWriteFile;
	unsigned long long receivefilepointer;
	unsigned long long receivefilesize;
	MemoryBuffer replyBuffer;
	BOOL bfiletransfermode = false;
	MemoryBuffer fileTransferBuffer;
	unsigned long ltempsize = 131072;

	while (true)
	{
		if (bfiletransfermode == true) {
			// File transfer mode - everything received will get written to the specified file
			// until the entire file has been received.
			res = recv (current_client, (char *) fileTransferBuffer.GetBuffer (), fileTransferBuffer.GetSize (), 0); // receive the data

			//Sleep (10);

			if (res == SOCKET_ERROR) {
				lasterror = WSAGetLastError ();
				printf ("There was a socket error with id: %i\n", lasterror);
				ExitThread (0);
			}

			if (res == 0) {
				printf ("There was an error receiving from the client.");
				closesocket(current_client);
				ExitThread(0);
			}

			BOOL bTestwrite;
			DWORD dwBytesWritten;
			bTestwrite = WriteFile (hWriteFile, fileTransferBuffer.GetBuffer(), res, &dwBytesWritten, NULL);

			receivefilepointer+=res;

			//printf("Received file chunk. File pointer: %i64 of %i64\n", receivefilepointer, receivefilesize);

			if (receivefilepointer == receivefilesize) {
				printf("Complete file received.\n");
				CloseHandle (hWriteFile);
				bfiletransfermode = false;
			}

			if (receivefilepointer > receivefilesize) {
				printf("File received, but file pointer is greater than file size.\n");
				printf("ReceiveFilePointer: %i64\n", receivefilepointer);
				printf("ReceiveFileSize: %i64\n", receivefilesize);

				CloseHandle (hWriteFile);

				bfiletransfermode = false;
			}

		} else {

			if (bbulkmode == false) {
				// Non-bulk mode receive - we're only expecting a signature and bulk buffer size at this stage
				res = recv (current_client, (char *) initialBuffer.GetBuffer (), initialBuffer.GetSize (), 0); // receive the data

				//Sleep (10);

				if (res == SOCKET_ERROR) {
					lasterror = WSAGetLastError ();
					printf ("There was a socket error with id: %i\n", lasterror);
			
					ExitThread (0);
				}

				if (res == 0) {
					printf ("There was an error receiving from the client.");
					closesocket(current_client);
					ExitThread(0);
				}

				int sig1 = 0;
				int sig2 = 0;
				unsigned long finalbufsize = 0;
		
				unsigned long ipointer = 0;
				memcpy (&sig1, (BYTE *) initialBuffer.GetBuffer() + ipointer, sizeof (int));
				ipointer+=sizeof (int);

				memcpy (&sig2, (BYTE *) initialBuffer.GetBuffer() + ipointer, sizeof (int));
				ipointer+=sizeof (int);

				memcpy (&finalbufsize, (BYTE *) initialBuffer.GetBuffer() + ipointer, sizeof (unsigned long));
				ipointer+=sizeof (unsigned long);

				printf ("Res is: %i\n", res);
				printf ("Signature 1: %i\n", sig1);
				printf ("Signature 2: %i\n", sig2);

				if (sig1 == SIGNATUREONE && sig2 == SIGNATURETWO) {
					printf ("Signature is valid.\n");
					printf ("Bulk Buffer Size: %i\n", finalbufsize);

					bulkpointer = 0;
					bulksize = finalbufsize;
					bulkBuffer.SetSize (finalbufsize);
					bulkStorage.SetSize (finalbufsize);
					bbulkmode = true;
				}
		
			} else {
				// We're in bulk mode - receive everything to the already set up buffer which should have been done in non-bulk mode
				res = recv (current_client, (char *) bulkBuffer.GetBuffer (), bulkBuffer.GetSize (), 0); // receive the data

				bulkStorage.Append (bulkBuffer.GetBuffer(), res);
				bulkpointer+=res;

				printf("Received bulk chunk. %i bytes. Pointer is %i of %i\n", res, bulkpointer, bulksize);
				//Sleep (700);

				if (bulkpointer == bulksize) {
					// We've received all the data

					printf("Entire bulk buffer received. Back to non bulk mode.\n");
					//bulkStorage.SaveToFile ("C:\\Temp\\receivepicture.jpg");

					int msg;
					int ipointer = 0;

					memcpy (&msg, (BYTE *) bulkStorage.GetBuffer() + ipointer, sizeof (int));
					ipointer += sizeof (int);

					if (msg == PREPARE_FILETRANSFER) {
						printf ("Prepare file transfer message received.\n");

						unsigned long long filesize;

						memcpy (&filesize, (BYTE *) bulkStorage.GetBuffer() + ipointer, sizeof (unsigned long long));
						ipointer+=sizeof (unsigned long long);

						int fnamelen = 0;
						memcpy (&fnamelen, (BYTE *) bulkStorage.GetBuffer() + ipointer, sizeof (int));
						ipointer+=sizeof (int);

						char szFilename[SIZE_STRING];
						ZeroMemory (szFilename, SIZE_STRING);

						strncpy (szFilename, (char *) bulkStorage.GetBuffer() + ipointer, fnamelen);
						ipointer += fnamelen;

						printf("Incoming file size is: %i64\n", filesize);
						printf("Incoming filename: %s\n", szFilename);

						hWriteFile = CreateFile ("C:\\Temp\\ReceiveWriteFile.avi", GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
						receivefilepointer = 0;
						receivefilesize = filesize;

						fileTransferBuffer.Clear ();
						fileTransferBuffer.SetSize (ltempsize);

						bfiletransfermode = true;
				
						if (hWriteFile == INVALID_HANDLE_VALUE) {
							printf("There was a problem opening the new file for writing.\n");
						}

						// Send the client a reply so that it can trigger file sending
						int sig1 = SIGNATUREONE;
						int sig2 = SIGNATURETWO;
						int msgident = BEGINFILESEND;
			
						replyBuffer.Clear ();
						replyBuffer.SetSize (1000);
						replyBuffer.Append (&sig1, sizeof (int));
						replyBuffer.Append (&sig2, sizeof (int));
						replyBuffer.Append (&msgident, sizeof (int));
		
						Sleep (10);
						send (current_client, (char *) replyBuffer.GetBuffer(), replyBuffer.GetSize (), 0);

						printf("Client Reply sent and File transfer mode has been set - awaiting data...\n");
					}


					bulkBuffer.Clear ();
					bulkStorage.Clear ();
					bulkpointer = 0;

					bbulkmode = false;
				}
			} // bulkmode
		}
	}
}

DWORD WINAPI receive_cmds (LPVOID lpParam)
{
	printf ("Client thread created.\n");

	SOCKET current_client = (SOCKET) lpParam;

	char buf[100];
	char sendData[100];
	int res;
	int lasterror = 0;

	bool bfiletransfermode = false;
	HANDLE hWriteFile;
	unsigned long long receivefilepointer = 0;
	unsigned long long receivefilesize = 0;

	MemoryBuffer receiveBuffer;
	receiveBuffer.SetSize (1000);

	MemoryBuffer replyBuffer;
	replyBuffer.SetSize (1000);

	// our receive loop
	while (true)
	{
		res = recv (current_client, (char *) receiveBuffer.GetBuffer (), receiveBuffer.GetSize (), 0); // receive the data

		Sleep (10);

		if (res == SOCKET_ERROR) {
			lasterror = WSAGetLastError ();
			printf ("There was a socket error with id: %i\n", lasterror);
			
			ExitThread (0);
		}

		if (res == 0) {
			printf ("There was an error receiving from the client.");
			closesocket(current_client);
			ExitThread(0);
		}

		if (bfiletransfermode == false) {
						
			// Deserialize the buffer
			// sig1 (int) | sig2 (int) | msg (int) | DATA (depends on msg)
			int sig1 = 0;
			int sig2 = 0;
			int msg = 0;
		
			unsigned long ipointer = 0;
			memcpy (&sig1, (BYTE *) receiveBuffer.GetBuffer() + ipointer, sizeof (int));
			ipointer+=sizeof (int);

			memcpy (&sig2, (BYTE *) receiveBuffer.GetBuffer() + ipointer, sizeof (int));
			ipointer+=sizeof (int);

			memcpy (&msg, (BYTE *) receiveBuffer.GetBuffer() + ipointer, sizeof (int));
			ipointer+=sizeof (int);

			//printf ("Buffer received: %s\n", buf);
			printf ("Signature 1: %i\n", sig1);
			printf ("Signature 2: %i\n", sig2);
			printf ("Message: %i\n", msg);

			if (sig1 == SIGNATUREONE && sig2 == SIGNATURETWO) {
				printf ("Signature is valid.\n");

				if (msg == TEST_MESSAGE) {
					printf ("Test Message Identifier received.\n");

					// Now we need the message size
					int msgsize = 0;
					memcpy (&msgsize, (BYTE *) receiveBuffer.GetBuffer() + ipointer, sizeof (int));
					ipointer+=sizeof (int);

					char szMessage[1024];
					ZeroMemory (szMessage, 1024);
					strncpy (szMessage, (char *) receiveBuffer.GetBuffer() + ipointer, msgsize);
					printf ("Message: %s\n", szMessage);
				}

				if (msg == PREPARE_FILETRANSFER) {
				
					printf ("Prepare file transfer message received.\n");

					unsigned long long filesize;

					memcpy (&filesize, (BYTE *) receiveBuffer.GetBuffer() + ipointer, sizeof (unsigned long long));
					ipointer+=sizeof (unsigned long long);

					int fnamelen = 0;
					memcpy (&fnamelen, (BYTE *) receiveBuffer.GetBuffer() + ipointer, sizeof (int));
					ipointer+=sizeof (int);

					char szFilename[SIZE_STRING];
					ZeroMemory (szFilename, SIZE_STRING);

					strncpy (szFilename, (char *) receiveBuffer.GetBuffer() + ipointer, fnamelen);
					ipointer += fnamelen;

					printf("Incoming file size is: %i64\n", filesize);
					printf("Incoming filename: %s\n", szFilename);

					hWriteFile = CreateFile ("C:\\Temp\\ReceiveWriteFile.avi", GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
					receivefilepointer = 0;
					receivefilesize = filesize;

					bfiletransfermode = true;
				
					if (hWriteFile == INVALID_HANDLE_VALUE) {
						printf("There was a problem opening the new file for writing.\n");
					}

					// Send the client a reply so that it can trigger file sending
					int sig1 = SIGNATUREONE;
					int sig2 = SIGNATURETWO;
					int msgident = BEGINFILESEND;
			
					replyBuffer.Clear ();
					replyBuffer.SetSize (1000);
					replyBuffer.Append (&sig1, sizeof (int));
					replyBuffer.Append (&sig2, sizeof (int));
					replyBuffer.Append (&msgident, sizeof (int));
		
					Sleep (10);
					send (current_client, (char *) replyBuffer.GetBuffer(), replyBuffer.GetSize (), 0);

					printf("Client Reply sent and File transfer mode has been set - awaiting data...\n");
				
				}
			}


		} //bfiletransfermode == false

		if (bfiletransfermode == true) {
			BOOL bTestwrite;
			DWORD dwBytesWritten;
			bTestwrite = WriteFile (hWriteFile, receiveBuffer.GetBuffer(), res, &dwBytesWritten, NULL);

			receivefilepointer+=res;

			printf("Received file chunk. File pointer: %i64\n", receivefilepointer);

			if (receivefilepointer == receivefilesize) {
				printf("Complete file received.\n");
				CloseHandle (hWriteFile);
			}

		}	

		//strcpy (sendData, "This is a reply");
		//Sleep (10);
		//send (current_client, sendData, sizeof (sendData), 0);
	}
}

int StartServer ()
{

	printf ("Starting up TCP Server....");
	SOCKET sock;

	DWORD dwThread;

	WSADATA wsaData;
	sockaddr_in server;

	int ret = WSAStartup (0x101, &wsaData);

	if (ret != 0) {
		return 0;
	}

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port=htons(7009);

	sock = socket(AF_INET, SOCK_STREAM, 0);

	if (sock == INVALID_SOCKET)
	{
		return 0;
	}

	if (bind (sock, (sockaddr *) &server, sizeof (server)) != 0) {
		return 0;
	}

	if (listen (sock, 5) != 0) {
		return 0;
	}

	SOCKET client;
	sockaddr_in from;
	int fromlen = sizeof (from);

	while (true)
	{
		client = accept (sock, (struct sockaddr *) &from, &fromlen);
		//break;
		printf ("Client connected.\n");
		CreateThread (NULL, 0, receive_cmds_ex, (LPVOID)client, 0, &dwThread);
	}

	closesocket (sock);
	WSACleanup ();

	return 0;
}

int StartClientEx ()
{
	int res = 0;
	int receivebytes = 0;

	sockaddr_in ser;
	sockaddr addr;
	WSADATA data;
	SOCKET sock;
	SOCKET client = NULL;

	ser.sin_family=AF_INET;
	ser.sin_port=htons(123);
	ser.sin_addr.s_addr=inet_addr("127.0.0.1");

	memcpy (&addr, &ser, sizeof (SOCKADDR_IN));

	res = WSAStartup (MAKEWORD (1, 1), &data);

	if (res != 0) {
		printf ("Client WSAStartup failed: %i\n", WSAGetLastError ());
	}

	sock=socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	if (sock==INVALID_SOCKET) {
		printf("Invalid Socket: %i\n", WSAGetLastError());
	}

	if (sock==SOCKET_ERROR) {
		printf("Socket Error: %i\n", WSAGetLastError ());
	}

	printf ("Socket Established\n");

	res=connect (sock, &addr, sizeof (addr));

	if (res != 0) {
		printf ("Server unavailable\n");
	} else {
		printf ("Connected to server\n");

		//char sendData[100];
		//ZeroMemory (sendData, 100);
		//strcpy (sendData, "This is send data.");
		MemoryBuffer filetosend;
		filetosend.ReadFromFile ("C:\\temp\\picture.jpg");
		
		printf("Done reading from file.\n");

		MemoryBuffer sendbuffer;

		int sig1 = SIGNATUREONE;
		int sig2 = SIGNATURETWO;
		unsigned long bulksize = filetosend.GetSize ();

		sendbuffer.SetSize ((sizeof (int) * 2) + sizeof (unsigned long) + filetosend.GetSize ());
		sendbuffer.Append (&sig1, sizeof (int));
		sendbuffer.Append (&sig2, sizeof (int));
		sendbuffer.Append (&bulksize, sizeof (unsigned long));
		sendbuffer.Append (filetosend.GetBuffer(), filetosend.GetSize ());
		
		printf("Sending data...\n");
		res = send (sock, (char *) sendbuffer.GetBuffer (), sendbuffer.GetSize (), 0);

		printf("Send complete.\n");
		// Now we await our reply to begin sending data



		printf("About to send again...");
		Sleep (5000);

		printf("Sending data...\n");
		res = send (sock, (char *) sendbuffer.GetBuffer (), sendbuffer.GetSize (), 0);

		printf("Send complete.\n");


		printf("And again...");
		Sleep (5000);

		printf("Sending data...\n");
		res = send (sock, (char *) sendbuffer.GetBuffer (), sendbuffer.GetSize (), 0);

		printf("Send complete.\n");

		printf("Closing socket...");
		Sleep (5000);

		if (res == 0)
		{
			printf ("Server terminated connection\n");
			Sleep (40);
			closesocket (sock);

		}
		else if (res == SOCKET_ERROR)
		{
			printf ("Socket error\n");
			Sleep (40);
		}
		closesocket (sock);
	}

	return 0;
}

int StartClient ()
{
	int res = 0;
	int receivebytes = 0;

	sockaddr_in ser;
	sockaddr addr;
	WSADATA data;
	SOCKET sock;
	SOCKET client = NULL;

	ser.sin_family=AF_INET;
	ser.sin_port=htons(7009);
	ser.sin_addr.s_addr=inet_addr("192.168.1.101");

	memcpy (&addr, &ser, sizeof (SOCKADDR_IN));

	res = WSAStartup (MAKEWORD (1, 1), &data);

	if (res != 0) {
		printf ("Client WSAStartup failed: %i\n", WSAGetLastError ());
	}

	sock=socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	if (sock==INVALID_SOCKET) {
		printf("Invalid Socket: %i\n", WSAGetLastError());
	}

	if (sock==SOCKET_ERROR) {
		printf("Socket Error: %i\n", WSAGetLastError ());
	}

	printf ("Socket Established\n");

	res=connect (sock, &addr, sizeof (addr));

	if (res != 0) {
		printf ("Server unavailable\n");
	} else {
		printf ("Connected to server\n");

		//char sendData[100];
		//ZeroMemory (sendData, 100);
		//strcpy (sendData, "This is send data.");

		// Get the file details
		HANDLE hFile;
		unsigned long long lsize = 0;
		unsigned long long lpointer = 0;
		LARGE_INTEGER lFileSize;

		char szFilename[SIZE_STRING];
		ZeroMemory (szFilename, SIZE_STRING);
		strcpy (szFilename, "C:\\Temp\\TestFile.avi");

		int filenamelen = strlen (szFilename);

		hFile = CreateFile (szFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		
		if (hFile == INVALID_HANDLE_VALUE) {
			printf ("There was a problem opening the file.\n");
		} else {
			GetFileSizeEx (hFile, &lFileSize);
			lsize = lFileSize.QuadPart;
			
			printf ("File opened ok.\n");
			printf ("File size: %i64\n", lsize);
		}
		

		printf ("About to send data....");
		Sleep (5000);

		int lasterror;
		MemoryBuffer sendbuffer;
		MemoryBuffer replyBuffer;
		
		// TEST_MESSAGE
		// sig1 (int) | sig2 (int) | msg (int) | DATA (depends on msg)

		// PREPARE_FILETRANSFER
		// sig1 (int) | sig2 (int) | msg (int) | filesize (ulonglong) | fnamelen (int) | fnamedata (char *)

		sendbuffer.SetSize ((sizeof (int) * 4) + sizeof (unsigned long long) + strlen (szFilename) + sizeof (unsigned long));

		int sig1 = SIGNATUREONE;
		int sig2 = SIGNATURETWO;
		int msgident = PREPARE_FILETRANSFER;
		unsigned long usize = sendbuffer.GetSize () - sizeof (unsigned long) - sizeof (int) - sizeof (int);

		sendbuffer.Append (&sig1, sizeof (int));
		sendbuffer.Append (&sig2, sizeof (int));
		sendbuffer.Append (&usize, sizeof (unsigned long));
		sendbuffer.Append (&msgident, sizeof (int));
		sendbuffer.Append (&lsize, sizeof (unsigned long long));
		sendbuffer.Append (&filenamelen, sizeof (int));
		sendbuffer.Append (szFilename, strlen (szFilename));

		printf ("Sendbuffer size is: %i\n", sendbuffer.GetSize ());
		printf ("USize is: %i\n", usize);

		res = send (sock, (char *) sendbuffer.GetBuffer (), sendbuffer.GetSize (), 0);

		// Now we await our reply to begin sending data
		printf("Sent a File Prep message. Awaiting reply from server...\n");

		replyBuffer.SetSize (1000);
		receivebytes = recv (sock, (char *) replyBuffer.GetBuffer (), replyBuffer.GetSize (), 0); // should block until we receive

		// We've received something
		if (receivebytes == SOCKET_ERROR) {
			lasterror = WSAGetLastError ();
			printf ("There was a socket error with id: %i\n", lasterror);
		}

		if (receivebytes == 0) {
			printf ("There was an error receiving from the server.");
		}

		int rsig1 = 0;
		int rsig2 = 0;
		int rmsg = 0;
		DWORD dwNumRead;
		BOOL bresreadfile;
		
		unsigned long ipointer = 0;
		memcpy (&rsig1, (BYTE *) replyBuffer.GetBuffer() + ipointer, sizeof (int));
		ipointer+=sizeof (int);

		memcpy (&rsig2, (BYTE *) replyBuffer.GetBuffer() + ipointer, sizeof (int));
		ipointer+=sizeof (int);

		memcpy (&rmsg, (BYTE *) replyBuffer.GetBuffer() + ipointer, sizeof (int));
		ipointer+=sizeof (int);

		printf ("Received Signature 1: %i\n", rsig1);
		printf ("Received Signature 2: %i\n", rsig2);
		printf ("Received Message: %i\n", rmsg);

		if (rsig1 == SIGNATUREONE && rsig2 == SIGNATURETWO) {
			
			printf ("Signature is valid.\n");
		
			if (rmsg == BEGINFILESEND) {
				printf("Server says it is ready for the file...\n");
				
				lpointer = 0;

				while (lpointer < lsize) {
					sendbuffer.Clear ();
					sendbuffer.SetSize (131072);
					
					bresreadfile = ReadFile (hFile, sendbuffer.GetBuffer(), 131072, &dwNumRead, NULL);	
					send (sock, (char *) sendbuffer.GetBuffer(), dwNumRead, 0);
					printf("Sent file chunk. Filepointer is: %i64\n", lpointer);
					lpointer+=dwNumRead;
					//Sleep (10);
				}

				// If we get here the file has been sent.
				CloseHandle (hFile);
			}
		}




		printf("Client is done. About to close socket...");
		Sleep (5000);

		if (res == 0)
		{
			printf ("Server terminated connection\n");
			Sleep (40);
			closesocket (sock);

		}
		else if (res == SOCKET_ERROR)
		{
			printf ("Socket error\n");
			Sleep (40);
		}
		closesocket (sock);
	}

	return 0;
}

